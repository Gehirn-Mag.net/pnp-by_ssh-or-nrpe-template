A clever template for PNP which selects the correct template for by_ssh or nrpe.

Select the appropriate template based on the name of the rrd file. If this was 
not found, simply use the default template.

More details (german only): https://gehirn-mag.net/icinga2-vs-pnp/

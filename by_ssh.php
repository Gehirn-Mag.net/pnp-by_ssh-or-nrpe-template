<?php

#
# A clever template for PNP which selects the correct template for by_ssh or 
# nrpe. Just rename or link the file to nrpe.php if needed ;-)
#

#
# MIT License
#
# Copyright (c) 2019 Steffen Schoch <mein@gehirn-mag.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


// Adjust these both to your needs
$templatePath = '/etc/pnp4nagios/templates';
$templateDefault = '/usr/share/pnp4nagios/html/templates.dist/default.php'; 

// Check if template for the used rrd file exists
if(preg_match('/([^\/]+)\.rrd$/', $RRDFILE[1], $matches)) {
    // Some debug information if needed
    //throw new Kohana_exception(print_r($matches,TRUE));
    $templateFile = $templatePath . '/' . $matches[1] . '.php';
    if(is_file($templateFile)) {
        // template found, so use it
        include($templateFile);
    } else {
        // nothing found, use default template
        include($templateDefault);
    }
}